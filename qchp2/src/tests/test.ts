import * as request from "request"
import { Promise } from "es6-promise"
import { readFileSync } from "graceful-fs"

const Session = request.defaults({
   jar: true,
   headers: {
      "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.86 Safari/537.36"
      // ,"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
      ,"Host": "www.qchp.org.qa"
      ,"Origin": "http://www.qchp.org.qa"
      ,"X-MicrosoftAjax": "Delta=true"
   }
})
   
function Request(options: any): Promise<string> {
   return new Promise<string>((resolve, reject): void => {
      Session(options, (err: any, resp: request.RequestResponse, body: string): void => {
         if (err) {
            reject(err.code)
         } else if (resp.statusCode !== 200) {
            reject(resp.statusCode)
         } else {
            resolve(body)
         }
      })
   })
}

Request({
   method: "POST"
   ,url: "http://www.qchp.org.qa/en/Pages/SearchPractitionersPage.aspx"
   ,form: readFileSync(`${__dirname}/../../formData.json`, "utf8")
})
.then((body: string) => { console.log(body) })
.catch((error: any) => { console.log(error) })

// Request({
//    method: "GET"
//    ,url: "http://www.qchp.org.qa/en/Pages/SearchPractitionersPage.aspx"
//    ,form: readFileSync(`${__dirname}/../../formData.json`, "utf8")
// })
// .then((body: string) => { console.log(body) })
// .catch((error: any) => { console.log(error) })
