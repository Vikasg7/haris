/// <reference path="../index.d.ts" />

import * as yargs from "yargs"
import { fork, execSync, ChildProcess } from "child_process"
import { LineReader } from "line-reader"
import { CreateStream } from "csv-write-stream"
import { Logger } from "./logger"

function StartWorker(onMessage: onWorkerMessage<ChildProcess>, seconds: string): void {
   const worker = fork(`${__dirname}/worker.js`, [seconds])
   worker.on("message", (msg: msg) => onMessage[msg.action](msg, worker))
   worker.on("error", onMessage.onWorkerError)
   worker.on("exit", onMessage.onExited)
}

export function init(): void {
   const args: ProcessArgs =  yargs
      .usage("Usage: npm init [Options]")
      .options({
         i: {
            alias: "in",
            describe: "File path to input text file.\nUse './' for current folder path",
         },
         o: {
            alias: "out",
            describe: "File path to input text file.\nUse './' for current folder path",
            default: `${__dirname}/../../output/${Date.now()}.csv`
         },
         f: {
            alias: "from",
            describe: "Serial number of line to start from.",
            default: 0
         },
         p: {
            alias: "threads",
            describe: "No. of workers to start.",
            default: 1
         },
         s: {
            alias: "seconds",
            describe: "Delay between each request in no. of seconds",
            default: "1"
         }
      })
      .demandOption(["i"])
      .help()
      .epilog("Copyright Vikas Gautam <v33vikasgautam@gmail.com>")
      .argv

   // Alternative to switch statement and long if else if constructs.
   const onMessage: onWorkerMessage<ChildProcess> = {
      SendNext(msg, worker): void {
         const url = urls.next().value
         if (url) {
            Logger.DoneUrls += 1
            Logger.Log()
            worker.send({action: "Scrape", url: url})
         } else {
            worker.send({action: "KillWorker"})
         }
      },
      
      Data(msg: msg): void {
         for (const row of msg.rows) {
            writer.write(row)
         }
      },

      onExited(): void {
         Logger.DoneWorkers += 1
         Logger.Log()
      },

      onWorkerError(error: any): void {
         Logger.DoneWorkers += 1
         Logger.ErrorCount += 1
         Logger.WorkerErrors  += 1
         Logger.LogError(error.toString())
      },

      LogError(msg: msg): void {
         Logger.ErrorCount += 1
         Logger.LogError(`${msg.url} | ${msg.error.toString()}`)
      }
   }

   const writer = CreateStream(args.o)
   process.on("exit", () => writer.end())

   const urls: IterableIterator<string> = LineReader(args.i, args.f)

   // Starting things off
   console.log("\n")
   Logger.DoneUrls += args.f
   Logger.TotalUrls += parseInt(execSync(`wc -l ${ args.i }`).toString()) + 1

   while (args.p !== 0) {
      StartWorker(onMessage, args.s)
      Logger.TotalWorkers += 1
      args.p -= 1
   }

   Logger.Log()
}