/// <reference path="../index.d.ts" />

import { load } from "cheerio"
import { Promise } from "es6-promise"
import * as request from "request"

String.prototype.strip = function (): string {
   return this.trim().replace(/\s{2,}/g, " ")
}

const Session = request.defaults(<request.CoreOptions>{
   jar: true,
   headers: {
      "User-agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.87 Safari/537.36"
   }
})

function Request(options: any): Promise<string> {
   return new Promise<string>((resolve, reject): void => {
      Session(options, (err: any, resp: request.RequestResponse, body: string): void => {
         if (err) {
            reject(err.code)
         } else if (resp.statusCode !== 200) {
            reject(resp.statusCode)
         } else {
            resolve(body)
         }
      })
   })
}

// define your scraping logic here
export function Scraper(url: string): Promise<Array<any>> {
   return Request(url)
      .then(Scrape.bind(null, url));
}

function Scrape(url: string, body: string): Array<any>|void {
   const $ = load(body)
   const rows: Array<any> = []

   const tblRows = $("table.SearchPractitionerGrid tr")
   if (!tblRows.length) return
   if ($(tblRows).text().strip() === "No Search Results found") return

   const isPaginated = $("table.SearchPractitionerGrid tr td[colSpan='5']").length

   $(tblRows).each((i, tblRow) => {
      const row = {url, isPaginated,lType: <string>undefined, name: <string>undefined, onClick: <string>undefined, workPlace: <string>undefined, scope: <string>undefined, lNum: <string>undefined, lExDate: <string>undefined}
      const cells = $(tblRow).find("td")

      const a = $(cells[0]).find("a")
      if (!a.length) { // it means Provisional Licenses
         row.lType = "Provisional"
      } else {
         row.lType = "Medical"
         row.onClick = $(a).attr("onclick")
      }
      row.name = $(cells[0]).text().strip()
      row.workPlace = $(cells[1]).text().strip()
      row.scope = $(cells[2]).text().strip()
      row.lNum = $(cells[3]).text().strip()
      row.lExDate = $(cells[4]).text().strip()
      rows.push(row)
   })

   return rows
}