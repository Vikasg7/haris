var gulp = require("gulp");
var ts = require("gulp-typescript");
var tsProject = ts.createProject("tsconfig.json");
var del = require("del")

var paths = {
    src: "./src/**/*",
    dist: "./dist"
}

function build() {
    return tsProject.src()
        .pipe(tsProject())
        .js.pipe(gulp.dest(paths.dist));
}

gulp.task("clean", function () {
    return del([paths.dist])
})

gulp.task("default", ["clean"], build);

gulp.task('watch', function () {
    gulp.watch(paths.src, ['default']);
});