const request = require("request")
const cheerio = require("cheerio")
const { log } = require("console")
const yargs = require("yargs")
const Rx = require("rxjs")
const { range } = Rx
const { map, flatMap, catchError, take } = require("rxjs/operators")
const { tap, createReqMaker, writeCsv } = require("rx-utils")

const session = request.defaults({
   jar: true,
   followAllRedirects: true,
   timeout: 1 * 60 * 1000,
   headers: {
      "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36"
   },
   encoding: "utf-8"
})

const makeReq = createReqMaker(session)
const strip = (str) => str.replace(/\s{2,}/g, " ").trim()

const equalsTextMaker = ($) => (selector, textOrRegex, isCaseSensitive = false) => {
   return $(selector).filter((i, ele) => {
      var val = $(ele).text() || ele.nodeValue
      if (textOrRegex instanceof RegExp) {
         return textOrRegex.test(val)
      } else if (isCaseSensitive) {
         return val === textOrRegex
      } else {
         return val.toLowerCase() === textOrRegex.toLowerCase()
      }
   })
}

const HOST = "https://services.dha.gov.ae/"

const scrape = (url) => (html) => {
   const $ = cheerio.load(html)
   const getTextS = (selector) => strip($(selector).text())
   const getTextE = (ele) => strip(ele.text())
   const equalText = equalsTextMaker($)
   const specialities = equalText(".section-header", "Specialities").eq(0).next(".section-body")
   const langs = equalText(".section-header", "Languages").eq(0).next(".section-body")
   // log("Languages: " + langs.length)
   const checkIfNotLicensed = (txt) => txt === "Not Licensed" ? "" : txt
   const row = {
      url,
      name: getTextS(".profile-header h1.accent-2"),
      telephone: getTextS(".profile-header .icon-Telephone"),
      country: getTextS(".profile-header .icon-Flag"),
      email: getTextS(".profile-header .icon-Envelope"),
      speciality: getTextE($("h4", specialities)),
      facility: checkIfNotLicensed(getTextE($(".profileBlue", specialities))),
      license: strip(getTextE($(".profileGrey", specialities)).split(":")[1] || ""),
      languages: $(".row.bot .large", langs).map((i, ele) => getTextE($(ele))).get().join(",")
   }

   // Experience
   const exps = equalText(".section-header", "Experience").eq(0).parent().find(".section-body")
   exps.each((i, exp) => {
      row["experience" + i] = strip($(exp).text()) 
   })

   // Education
   const edus = equalText(".section-header", "Education").eq(0).parent().find(".section-body")
   edus.each((i, edu) => {
      row["Education" + i] = strip($(edu).text())
   })

   return [row]
}

const FORM = {
   "string": "", 
   "speciality": [], 
   "category": [], 
   "facilityName": [], 
   "pageNo": 1, 
   "pageSize": "25", 
   "area": [], 
   "licenseType": [], 
   "nationality": [], 
   "gender": [], 
   "sortBy": "First Name", 
   "languages": [], 
   "locale": "en"
}

const assoc = (key, value, obj) => {
   obj[key] = value
   return obj
}

const main = (args) =>
   makeReq("https://services.dha.gov.ae/eservices/dhaweb/default.aspx").pipe(
      tap(_ => makeReq("https://services.dha.gov.ae/sheryan/wps/portal/home/medical-directory?professional=&locale=en")),
      flatMap(_ => range(1, 1808)),
      flatMap(p => makeReq({
         method: "POST",
         url: "https://services.dha.gov.ae/sheryan/RestService/rest/retrieve/medicaldirectorysearchdetails?key=SHARED_KEY",
         headers: {
            "Content-Type": "application/json"
         },
         body: `{"string":"","speciality":[],"category":[],"facilityName":[],"pageNo":${p},"pageSize":"25","area":[],"licenseType":[],"nationality":[],"gender":[],"sortBy":"First Name","languages":[],"locale":"en"}`
      }).pipe(
         catchError(e => { log(e, p); return Rx.EMPTY })
      ), 2),
      map(JSON.parse),
      flatMap(data => data.professionalSearch.professionalsDTO.professionalsDTO.map(x => x.dhaUniqueId)),
      map(uId => "https://services.dha.gov.ae/sheryan/wps/portal/home/medical-directory/professional-details?dhaUniqueId=" + uId),
      flatMap((url, i) => makeReq(url).pipe(
         tap(_ => log(i, url)),
         flatMap(scrape(url)),
         catchError(e => {log(e, url); return Rx.EMPTY})
      ), 8),
      // take(20),
      writeCsv(args.o)
   )

const getArgs = () =>
   yargs
      .usage("node index.js [Options]")
      .options({
         "o": {
            type: "string",
            demandOption: true,
            describe: "Output file Path",
            alias: "out-file"
         }
      })
      .help()
      .argv

const args = getArgs()

main(args).subscribe(null, log)