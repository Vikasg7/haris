const request = require("request")
const cheerio = require("cheerio")
const { log } = require("console")
const yargs = require("yargs")
const Rx = require("rxjs")
const { map, flatMap, catchError, delay, concatMap, reduce } = require("rxjs/operators")
const { tap, createReqMaker, CreateWriter, finalyze, tapOnComplete } = require("rx-utils")
const puppeteer = require("puppeteer")

const session = request.defaults({
   jar: true,
   followAllRedirects: true,
   timeout: 1 * 60 * 1000,
   headers: {
      "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36"
   },
   encoding: "utf-8"
})

const makeReq = createReqMaker(session)
const strip = (str) => str.replace(/\s{2,}/g, " ").trim()

const containsMaker = ($) => (selector, needle) =>
   $(selector).filter((i, ele) => $(ele).text().includes(needle))

const scrape = (html) => {
   const $ = cheerio.load(html)

   const getTextS = (selector) => strip($(selector).text())
   const getTextE = (ele) => strip(ele.text())
   const contains = containsMaker($)
   const getData = (head) => strip(contains(".facilitylabel span", head).parent().contents()[2].data)

   const row = {
      url: $("link[rel='canonical']").attr("href"),
      name: getTextS(".facilitydetailname h3"),
      location: getTextS(".facilitydetailname .useremail"),
      description: getTextS(".facility-info"),
      telephone: getData("Telephone Number"),
      fax: getData("Fax Number"),
      workingHours: getData("Working Hours :"),
      workingHoursWeekend: getData("Working Hours (Weekend)"),
      poBox: getData("P.O Box")
   }

   return [row]
}

const getDocLinks = (html) => {
   const $ = cheerio.load(html)
   return $("div.view-profile a").map((i, a) => $(a).attr("href")).get()
}

const isNextDisabled = (html) => {
   const $ = cheerio.load(html)
   return $("a.aspNetDisabled.nextpage.page-next").length > 0
}

const gotoNextPage = (page) =>
   Rx.from(page.click("a.nextpage.page-next")).pipe(
      tap(() => page.waitForNavigation({timeout: 0})),
      delay(1000)
   )

const logErrorAndIgnore = (source) =>
   source.pipe(
      catchError(e => { log(e); return Rx.EMPTY })
   )

const scrapeDocLinks = (page) => 
   Rx.from(page.content()).pipe(
      tap(html => Rx.of(html).pipe(
         flatMap(getDocLinks),
         concatMap(makeReq),
         flatMap(scrape),
         // tap((row, i) => log(i, row.name)),
         tap(log),
         map((row) => writer.next(row)),
         logErrorAndIgnore,
         reduce((_, v) => v, null),
      )),
      tap(html => isNextDisabled(html) ? Rx.EMPTY : gotoNextPage(page).pipe(concatMap(() => scrapeDocLinks(page))))
   )

const subMain = (browser) =>
   Rx.from(browser.newPage()).pipe(
      tap((page) => page.goto("https://www.seha.ae/SKMC/English/MedicalServices/Pages/Find-a-facility.aspx", {timeout: 0})),
      delay(30000),
      flatMap(scrapeDocLinks),
      finalyze(() => browser.close()),
      finalyze(() => writer.complete())
   )

const main = () => Rx.from(puppeteer.launch({headless: false})).pipe(flatMap(subMain))

const getArgs = () =>
   yargs
      .usage("node index.js [Options]")
      .options({
         "o": {
            type: "string",
            demandOption: true,
            describe: "Output file Path",
            alias: "out-file"
         }
      })
      .help()
      .argv

const args = getArgs()

const writer = CreateWriter(args.o)

main().subscribe(null, log)