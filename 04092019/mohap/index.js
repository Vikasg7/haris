const request = require("request")
// require("request-debug")(request)
const cheerio = require("cheerio")
const { log } = require("console")
const yargs = require("yargs")
const Rx = require("rxjs") 
const { map, flatMap, catchError, filter, retry } = require("rxjs/operators")
const { tap, createReqMaker, writeCsv } = require("rx-utils")
const R = {
   mergeAll: require("ramda/src/mergeAll"),
   pipe: require("ramda/src/pipe")
} 

const session = request.defaults({
   jar: true,
   followAllRedirects: true,
   timeout: 1 * 60 * 1000,
   headers: {
      "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36"
   },
   encoding: "utf-8"
})

const makeReq = createReqMaker(session)
const strip = (str) => str.replace(/\s{2,}/g, " ").trim()

const getOptValuesM = ($) => (eles) => eles.map((i, ele) => $(ele).attr("value")).get()

const cartesian = (as, bs, cs) => {
   const res = []
   as.forEach(a => bs.forEach(b => cs.forEach(c => res.push([a, b, c]))))
   return res
}

const getOptCombos = ($) => {
   const getOptValues = getOptValuesM($)
   const medDists = getOptValues($("div.contenthead select[name$='MedicalDistrict'] option"))
   const specility = getOptValues($("div.contenthead select[name$='Speciality'] option"))
   const medtitle = getOptValues($("div.contenthead select[name$='MedicalTitle'] option"))
   return cartesian(medDists, specility, medtitle)
}

const makeSoup = (html) => cheerio.load(html)
const assocBy = (needle, value) => (obj) => {
   Object.keys(obj).forEach(k => 
      k.includes(needle)
         ? obj[k] = value
         : null
   )
   return obj
}

const getForm = ($) => R.mergeAll(
   $("input[name]").map((i, el) => ({ [$(el).attr("name")]: $(el).val() })).get().concat(
      $(".contenthead select[name]").map((i, el) => ({ [$(el).attr("name")]: $(el).val() })).get()
   )
)

const HOST = "https://www.mohap.gov.ae/"

const scrape = (html) => {
   const $ = cheerio.load(html)
   const getTextS = (selector) => strip($(selector).text())
   const getTextE = (ele) => strip(ele.text())
   // const equalText = equalsTextMaker($)
   const speciality2 = getTextE($("select[id*='ddlSpeciality'] option[selected]"))
   const rows = []
   $("div.drdetails").each((i, div) => {
      const row = {
         name: getTextE($("span[id$='Name']", div)),
         facilty: getTextE($("span[id$='Facility']", div)),
         speciality: getTextE($("span[id$='Medical']", div)),
         nationality: getTextE($("span[id$='National']", div)),
         speciality2
      }
      rows.push(row)
   })
   return rows
}

const calc = (...fns) => (...args) => fns.map(fn => fn(...args))

const main = (args) =>
   makeReq("https://www.mohap.gov.ae/en/Pages/profdoctors.aspx").pipe(
      map(makeSoup),
      map(calc(getOptCombos, getForm)),
      flatMap(([combos, form], i) =>
         Rx.from(combos).pipe(
            filter(([a, b, c]) => !(a == "-1" || b == "-1" || c == "-1")),
            flatMap(([d, s, t]) => 
               makeReq({
                  method: "POST",
                  jar: false,
                  url: "https://www.mohap.gov.ae/en/Pages/profdoctors.aspx",
                  formData: R.pipe(
                     assocBy("MedicalDistrict", d),
                     assocBy("Speciality", s),
                     assocBy("MedicalTitle", t)
                  )(form)
               }).pipe(
                  retry(5),
                  catchError(e => { log(e); return Rx.EMPTY }),
                  tap(() => log(i, d, s, t))
               )
            , 6),
            flatMap(scrape)
         )
      ),
      tap((r, i) => log(i, r.name + " " + r.speciality)),
      writeCsv(args.o)
   )

const getArgs = () =>
   yargs
      .usage("node index.js [Options]")
      .options({
         "o": {
            type: "string",
            demandOption: true,
            describe: "Output file Path",
            alias: "out-file"
         }
      })
      .help()
      .argv

const args = getArgs()

main(args).subscribe(null, log)