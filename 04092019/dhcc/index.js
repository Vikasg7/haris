const request = require("request")
const cheerio = require("cheerio")
const { log } = require("console")
const yargs = require("yargs")
const Rx = require("rxjs")
const { range } = Rx
const { map, flatMap, catchError } = require("rxjs/operators")
const { tap, createReqMaker, writeCsv } = require("rx-utils")
const { resolve: UrlResolve } = require("url")

const session = request.defaults({
   jar: true,
   followAllRedirects: true,
   timeout: 1 * 60 * 1000,
   headers: {
      "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36"
   },
   encoding: "utf-8"
})

const makeReq = createReqMaker(session)
const strip = (str) => str.replace(/\s{2,}/g, " ").trim()

const equalsTextMaker = ($) => (selector, textOrRegex, isCaseSensitive = false) => {
   return $(selector).filter((i, ele) => {
      var val = $(ele).text() || ele.nodeValue
      if (textOrRegex instanceof RegExp) {
         return textOrRegex.test(val)
      } else if (isCaseSensitive) {
         return val === textOrRegex
      } else {
         return val.toLowerCase() === textOrRegex.toLowerCase()
      }
   })
}

const HOST = "http://www.dhcc.ae/"

const scrape = (html) => {
   const $ = cheerio.load(html)
   const getTextS = (selector) => strip($(selector).text())
   const getTextE = (ele) => strip(ele.text())
   // const equalText = equalsTextMaker($)
   const row = {
      telephone: getTextE($("i.icon16-tel").parent()),
      email: getTextE($("i.icon16-email").parent()),
      website: getTextE($("i.icon16-web").parent()),
      address: getTextS("li.address")
   }
   return [row]
}

const assoc = (key, value, obj) => {
   obj[key] = value
   return obj
}

const getClinicLink = (txt) => UrlResolve("http://www.dhcc.ae/Portal/", txt.match(/href='(.+?)'/)[1])
const getClinicName = (txt) => txt.match(/>(.*?)</)[1]

const main = (args) =>
   makeReq(HOST).pipe(
      tap(_ => makeReq("http://www.dhcc.ae/portal/en/healthcare/doctors.aspx#page=1")),
      flatMap(_ => range(1, 131)),
      flatMap(p => makeReq({
         method: "POST",
         url: "http://www.dhcc.ae/Portal/Services/AjaxHandler.asmx/LoadDoctorsList",
         headers: {
            "Content-Type": "application/json; charset=UTF-8"
         },
         body: `{"pageSize":10,"languageId":1,"languageCode":"en-GB","keywords":"","clinic":"","speciality":"","insuarance":"","gender":"","language":"","pageIndex":${p}}`
      }).pipe(
         catchError(e => { log(e, p); return Rx.EMPTY })
      ), 1),
      map(JSON.parse),
      flatMap(json => json.d.items),
      map(item => item.clinics ? assoc("clinic", getClinicName(item.clinics), item) : item),
      // tap(item => log(getClinicLink(item.clinics))),
      flatMap(item => 
         item.clinics
            ? makeReq(getClinicLink(item.clinics)).pipe(
               flatMap(scrape),
               catchError(e => { log(e, item.name); return Rx.of(item) }),
               map(c => ({...item, ...c}))
             )
            : Rx.of(item)
      , 1),
      tap((x, i) => log(i, x.name)),
      writeCsv(args.o)
   )

const getArgs = () =>
   yargs
      .usage("node index.js [Options]")
      .options({
         "o": {
            type: "string",
            demandOption: true,
            describe: "Output file Path",
            alias: "out-file"
         }
      })
      .help()
      .argv

const args = getArgs()

main(args).subscribe(null, log)