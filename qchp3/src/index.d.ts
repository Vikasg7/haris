declare module "readline" {
   export function cursorTo(stream: NodeJS.WritableStream, x: number, y?: number): void;
}

interface msg {
   action: string,
   rows?: Array<any>,
   error?: any,
   url?: string
}