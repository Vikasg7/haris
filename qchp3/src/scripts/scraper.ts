/// <reference path="../index.d.ts" />

import { load } from "cheerio"
import * as request from "request"
import { RequestAPI, Request, CoreOptions, RequiredUriUrl, Options } from "request"

declare global {
   interface String {
      strip(): string;
   }
}

String.prototype.strip = function (): string {
   return this.trim().replace(/\s{2,}/g, " ")
}

export class ScraperCL {
   private _session: RequestAPI<Request, CoreOptions, RequiredUriUrl>
   public host: string

   constructor() {
      this._session = request.defaults({
         jar: true,
         maxRedirects: 5,
         timeout: 10000,
         headers: {
            "User-agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.87 Safari/537.36"
         }
      })
      this.host = "http://www.qchp.org.qa/en/"
   }

   public request(options: Options|string): Promise<string> {
      return new Promise<string>((resolve, reject): void => {
         if (typeof options == "string") options = {url: options}
         this._session(options, (err: any, resp, body: string): void => {
            err ? reject(err.code) : resp.statusCode >= 400 ? reject(resp.statusCode): resolve(body)
         }).on("error", (error) => reject(error.toString()))
      })
   }

   private _scrape(context: any): Array<any>|void {
      const { body, url } = context
      const $ = load(body)
      const rows: Array<any> = []
      const row = {url}
      
      const tr = $("#ctl00_ctl47_g_580d9f75_ba31_477f_905b_bf6e7a2f8dd5_ctl00_gv_results tbody tr")
      if (tr.length === 1 || tr.eq(0).text().includes("No Search Results found")) {
         return
      }
      else {
         rows.push(row)
         return rows
      }
   }

   public Scraper(url: string): Promise<Array<any>|void> {
      return this.request(url)
         .then((body: string) => ({body, url}))
         .then((context: any) => this._scrape(context))
   }
}