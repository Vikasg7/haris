/// <reference path="../index.d.ts" />

import * as ReadLine from "readline"
import { createWriteStream, WriteStream } from "graceful-fs"

export class LoggerCL {
   private _logFile: WriteStream
   private _stdout = process.stdout
   public TotalUrls: number = 0
   public DoneUrls: number = 0
   public WorkerErrors: number = 0
   public ErrorCount: number = 0
   public TotalWorkers: number = 0
   public DoneWorkers: number = 0

   constructor() {
      this._logFile = this._getLogFile()
   }

   private _getLogFile() {
      const logFile = createWriteStream(`${__dirname}/../../logs/${Date.now()}.txt`)
      process.on("exit", () => logFile.end())
      return logFile
   }

   private _clear(): void {
      ReadLine.moveCursor(this._stdout, 0, -7)
      ReadLine.cursorTo(this._stdout, 0)
      ReadLine.clearScreenDown(this._stdout)
   }

   private _logProgress(): void {
      const speed = Math.round(this.DoneUrls / process.uptime() * 100)/100
      const stdMsg: Array<string> = [
         `Urls        : ${this.DoneUrls} / ${this.TotalUrls}`,
         `%Completion : ${parseInt((this.DoneUrls / this.TotalUrls * 100).toString())}%`,
         `Error Count : ${this.ErrorCount}`,
         `Workers     : ${this.DoneWorkers} / ${this.TotalWorkers}`,
         `Worker Errs : ${this.WorkerErrors}`,
         `Time Used   : ${this._getTime(process.uptime())}`,
         `Speed       : ${speed} Urls/sec`,
         `Time left   : ${this._getTime(Math.round((this.TotalUrls - this.DoneUrls) / speed))}`
      ]
      this._stdout.write(stdMsg.join("\n"))
   }

   private _getTime(seconds: number): string {
      if (Number.isNaN(seconds) || !Number.isFinite(seconds)) return ""
      const date = new Date(null)
      date.setSeconds(seconds)
      return date.toUTCString().match(/[0-9]{2}\:[0-9]{2}\:[0-9]{2}/g)[0]
   }

   public Log(): void {
      this._clear()
      this._logProgress()
   }

   public LogError(error: any): void {
      this._clear()
      console.log(error)
      this._logProgress()
      this._logFile.write(`${error}\n`)
   }
}