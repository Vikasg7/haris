/// <reference path="../index.d.ts" />

import * as yargs from "yargs"
import { Arguments } from "yargs"
import { fork, execSync, ChildProcess } from "child_process"
import { LineReader } from "line-reader"
import { CreateStream, CsvWriteStream } from "csv-write-stream"
import { LoggerCL } from "./logger"

declare module "yargs" {
   interface Arguments {
      i: string,
      o: string,
      f: number,
      p: number,
      d: string
   }
}

interface onWorkerMessage {
   SendNext(msg: msg, worker: ChildProcess): void;
   Data(msg: msg): void;
   onExited(): void;
   onWorkerError(error: any): void;
   LogError(msg: msg): void;
}

export class InitCL {
   private _urls: IterableIterator<string>
   private _writer: CsvWriteStream
   private _logger: LoggerCL
   private _args: Arguments
   private _onMessage: onWorkerMessage

   constructor() {
      this._args = this._getArgs()
      this._logger = new LoggerCL()
      this._urls = LineReader(this._args.i, this._args.f)
      this._writer = this._getWriter(this._args.o)
      this._onMessage = this._getOnMessage()
   }

   private _getArgs(): Arguments {
      return <Arguments>yargs
         .usage("Usage: npm init [Options]")
         .options({
            i: {
               alias: "in",
               describe: "File path to input text file.\nUse './' for current folder path",
            },
            o: {
               alias: "out",
               describe: "File path to input text file.\nUse './' for current folder path",
               default: `${__dirname}/../../output/${Date.now()}.csv`
            },
            f: {
               alias: "from",
               describe: "Serial number of line to start from.",
               default: 0
            },
            p: {
               alias: "threads",
               describe: "No. of workers to start.",
               default: 1
            },
            s: {
               alias: "seconds",
               describe: "Delay between each request in no. of seconds",
               default: "1"
            }
         })
         .demandOption(["i"])
         .help()
         .epilog("Copyright Vikas Gautam <v33vikasgautam@gmail.com>")
         .argv
   }

   private _getOnMessage(): onWorkerMessage {
      const _writer = this._writer
      const _logger = this._logger
      const _urls = this._urls
      return {
         SendNext(msg: msg, worker: ChildProcess): void {
            const url = _urls.next().value
            if (url) {
               _logger.DoneUrls += 1
               _logger.Log()
               worker.send({action: "Scrape", url: url})
            } else {
               worker.send({action: "KillWorker"})
            }
         },
         
         Data(msg: msg): void {
            for (const row of msg.rows) {
               _writer.write(row)
            }
         },

         onExited(): void {
            _logger.DoneWorkers += 1
            _logger.Log()
         },

         onWorkerError(error: any): void {
            _logger.DoneWorkers += 1
            _logger.ErrorCount += 1
            _logger.WorkerErrors  += 1
            _logger.LogError(error.toString())
         },

         LogError(msg: msg): void {
            _logger.ErrorCount += 1
            _logger.LogError(`${msg.url} | ${msg.error.toString()}`)
         }
      }
   } 

   private _startWorker(): void {
      const worker = fork(`${__dirname}/worker.js`, [this._args.s])
      worker.on("message", (msg: msg) => this._onMessage[msg.action](msg, worker))
      worker.on("error", this._onMessage.onWorkerError)
      worker.on("exit", this._onMessage.onExited)
   }

   private _getWriter(oFilePath: string): CsvWriteStream {
      const writer = CreateStream(oFilePath)
      process.on("exit", () => writer.end())
      return writer
   }

   public init() {
      // Starting things off
      console.log("\n")
      this._logger.DoneUrls += this._args.f
      this._logger.TotalUrls += parseInt(execSync(`wc -l ${ this._args.i }`).toString()) + 1

      while (this._args.p !== 0) {
         this._startWorker()
         this._logger.TotalWorkers += 1
         this._args.p -= 1
      }

      this._logger.Log()
      this._args = null
   }
}