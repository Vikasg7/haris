/// <reference path="../index.d.ts" />

import { ScraperCL } from "./scraper"

class WorkerCL {
   private _delayInMs: number
   private _scraperCL: ScraperCL

   constructor() {
      this._delayInMs = parseFloat(process.argv[2]) * 1000
      this._scraperCL = new ScraperCL()
   }

   private _sendNext(): void {
      process.send({action: "SendNext"})
   }

   private _logError(error: string, url: string): void {
      process.send({action: "LogError", error: (error ? error.toString() : ""), url: url})
   }

   private _sendData(rows: Array<any>): void {
      process.send({action: "Data", rows: rows})
   }

   private _wait(): Promise<void> {
      return this._delayInMs ? new Promise<void>((resolve, reject) => setTimeout(resolve, this._delayInMs)) : null
   }

   private _onMessage(msg: msg) {
      if (msg.action === "Scrape") {
         this._scraperCL.Scraper(msg.url)
            .then((rows: Array<any>|void) => rows && this._sendData(rows))
            .catch((error: any) => this._logError(error, msg.url))
            .then(() => this._wait())
            .then(() => this._sendNext())
      } else if (msg.action === "KillWorker") {
         process.exit(0)
      }
   }

   public init() {
      process.on("message", (msg: msg) => this._onMessage(msg))
      // Making a session and then bring first url
      this._scraperCL.request(this._scraperCL.host).then(() => this._sendNext())
   }
}

new WorkerCL().init()