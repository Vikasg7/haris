// parsing Arguments
if (process.argv.length < 3) {
   console.log(`
      Syntax:
         node dist/index.js <path-to-input-file> <path-to-output-file> <from>
      
      Arguments:
            <path-to-input-file> [required]
         <path-to-output-file> [required]
                        <from> (Optional)
      
      Example:
         node dist/index.js "input/input.txt" "output/output.txt" 15
   `)
   process.exit(1)
}

import { init } from "./scripts/scraper"
init()