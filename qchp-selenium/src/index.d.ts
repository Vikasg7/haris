interface Promise<T> {
   thenForEach<P>(doThis: (item: any, index: number, context: any) => any, onError?: (item: any, index: number, context: any, error: any) => any): Promise<P>
}

interface String {
   strip(): string
}

interface Cheerio {
   equalsText(textOrRegex: string|RegExp, isCaseSensitive: boolean, $: CheerioStatic): Cheerio
}
