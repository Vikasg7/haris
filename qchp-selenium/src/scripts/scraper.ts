/// <reference path="../index.d.ts" />

import { initClient } from "./client"
import { Options as WebDriverOptions, Client } from "webdriverio"
import { readFileSync } from "graceful-fs"
import { thenForEach } from "./thenForEach"
import * as cheerio from "cheerio"
import { load } from "cheerio"
const { CreateStream } = require("csv-write-stream")

Promise.prototype.thenForEach = thenForEach
String.prototype.strip = function (): string {
   return this.trim().replace(/\s{2,}/g, " ")
}
cheerio.prototype.equalsText = function (textOrRegex: string|RegExp, isCaseSensitive: boolean, $: CheerioStatic): Cheerio {
  return $(this).filter(function () {
     const val = $(this).text() || this.nodeValue
   if (textOrRegex instanceof RegExp)
      return textOrRegex.test(val)
   else if (isCaseSensitive)
      return val === textOrRegex
   else
      return val.toLowerCase() === textOrRegex.toLowerCase()
  })
}

const from = parseInt(process.argv[4] || "0")
export function init(): void {
   const options: WebDriverOptions = {
      desiredCapabilities: {
         browserName: "chrome"
      }
   }
   const inFile = process.argv[2]
   const urls = readFileSync(inFile, "utf8").trim().split(/\r\n|\n/).slice(from)
   initClient(options)
      .then((clientArr) => {
         const client = clientArr[0]
         return Crawl(urls, client)
            .then(() => <any>client.end())
            .then(() => console.log("Done!"))
      })
      .catch((error) => console.error(error))
}

function Crawl(urls: Array<string>, client: Client<void>): Promise<void> {
   return Promise.resolve(urls)
      .thenForEach<void>((url: string, index: number) => {
         console.log(from + index)
         const practType = url.match(/PractType=(.*?)\&/)[1]
         const placeOfWork = url.match(/PlaceOfWork=(.*)/)[1]
         cPage = 1 // resetting page counter
         return client
            .pause(5 * 1000) // to let browser start.. !important to avoid NoSessionId Error
            .url(url)
            .getSource()
            .then(Scrape.bind(null, url, client))
            .then(WriteToFile)
            .selectByValue("select[id*='drp_practitionerType']", practType)
            .pause(10 * 1000)
            .selectByValue("select[id*='drp_placeofwork']", placeOfWork)
            .selectByValue("select[id*='drp_placeofwork']", placeOfWork)
            .pause(5 * 1000)            
            .getSource()
            .then(CrawlNextPage.bind(null, url, client))
      })
      .catch((error) => console.error(error))
}

function Scrape(url: string, client: Client<void>, body: string): Array<any> {
   const $ = load(body)
   const rows: Array<any> = []

   const tblRows = $("table.SearchPractitionerGrid tr")
   if (!tblRows.length) return
   if ($(tblRows).text().strip() === "No Search Results found") return

   const isPaginated = $("table.SearchPractitionerGrid tr td[colSpan='5']").length

   $(tblRows).each((i, tblRow) => {
      const row = {url, isPaginated,lType: <string>undefined, name: <string>undefined, onClick: <string>undefined, workPlace: <string>undefined, scope: <string>undefined, lNum: <string>undefined, lExDate: <string>undefined}
      const cells = $(tblRow).find("td")

      const a = $(cells[0]).find("a")
      if (!a.length) { // it means Provisional Licenses
         row.lType = "Provisional"
      } else {
         row.lType = "Medical"
         row.onClick = $(a).attr("onclick")
      }
      row.name = $(cells[0]).text().strip()
      row.workPlace = $(cells[1]).text().strip()
      row.scope = $(cells[2]).text().strip()
      row.lNum = $(cells[3]).text().strip()
      row.lExDate = $(cells[4]).text().strip()
      rows.push(row)
   })
   return rows
}

const writer = CreateStream(process.argv[3])
process.on("exit", () => writer.end())

function WriteToFile(rows: Array<any>): void {
   for (const row of rows) {
      writer.write(row)
   }
}

let cPage = 1
const delayInSec = 20

function CrawlNextPage(url: string, client: Client<void>, body: string): Client<any> {
   const $ = load(body)

   const nextPageEle = $(`td a[href*='Page$${cPage + 1}']`)
   if (!nextPageEle.length) return

   cPage += 1
   return client
      .element(`td a[href*='Page$${cPage}']`)
      .click()
      .pause(delayInSec * 1000)
      .getSource()
      .then(Scrape.bind(null, url, client))
      .then(WriteToFile)
      .getSource()
      .then(CrawlNextPage.bind(null, url, client))
}