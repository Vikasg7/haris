
export type doThisFn = (item: any, index: number, context: any) => any
export type onErrorFn = (item: any, index: number, context: any, error: any) => any

export function thenForEach<T>(doThis: doThisFn, onError?: onErrorFn): Promise<T> {
   const Promise = this.constructor
   return this.then((array: any) => {
      let context: any
      // Mapping arguments
      if (!Array.isArray(array)) {
         context = array.context
         array = array.array
      }
      return (
         array.reduce((Promise: Promise<void>, item: number, index: number) => {
            return Promise
               .then(doThis.bind(null, item, index, context))
               .catch(onError && onError.bind(null, item, index, context))
         }, Promise.resolve())
         .then(((context: any) => { return context }).bind(null, context))
      )
   })
}