/// <reference path="../index.d.ts" />

import { del, get } from "request"
import * as Webdriverio from "webdriverio"
import { Options as WebDriverOptions, Client } from "webdriverio"

const SESSIONS_URL = "http://127.0.0.1:4444/wd/hub/sessions"
const SESSION_URL_PREFIX = "http://127.0.0.1:4444/wd/hub/session"

function DelSession(sessionId: string): Promise<void> {
   return new Promise<void>((reject, resolve) => {
      del(`${SESSION_URL_PREFIX}/${sessionId}`, {body: "{}"}, (err, resp, body) => {
         if (err) {
            reject(err.code)
         } else {
            resolve()
         }
      })
   })
}

interface GetSessionResp {
   value: Array<{id: string}>
}

function GetSession(): Promise<GetSessionResp> {
   return new Promise<GetSessionResp>((resolve, reject) => {
      get(SESSIONS_URL, (err, resp, body) => {
         if (err)
            reject(err.code)
         else if (resp.statusCode !== 200)
            reject(resp.statusCode)
         else 
            resolve(JSON.parse(body))
      })
   })
}

function StartNewClient(options: WebDriverOptions): Array<Client<any>> {
   return [Webdriverio.remote(options).init()]
}

function GetRunningClient(sessionId: string, options: WebDriverOptions): Promise<Array<Client<any>>> { 
   const client = Webdriverio.remote(<any>sessionId)
   return <any>client
      .getTitle() // Checking if client window exists
      .then(() => [client]) // returing client if so
      .catch(() => {
         return DelSession(sessionId) // del the current session with no browser window
            .then(() => StartNewClient(options)) // returning new client
      })
}

export function initClient(options: WebDriverOptions): Promise<Array<Client<any>>> {
   return GetSession()
      .then((resp: GetSessionResp) => {
         // means no session exists
         if (!resp.value.length)
            return StartNewClient(options)
         else
            return GetRunningClient(resp.value[0].id, options)         
      })
}