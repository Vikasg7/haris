interface String {
   strip(): string
}

declare module "readline" {
   export function cursorTo(stream: NodeJS.WritableStream, x: number, y?: number): void;
}

interface msg {
   action: string,
   rows?: Array<any>,
   error?: any,
   url?: string
}

interface ProcessArgs {
   i: string,
   o: string,
   f: number,
   p: number,
   s: string
}

interface onWorkerMessage<T> {
   SendNext(msg: msg, worker: T): void,
   Data(msg: msg): void,
   onExited(): void,
   onWorkerError(error: any): void,
   LogError(msg: msg): void,
   [key: string]: (msg?: msg, worker?: T) => void 
}