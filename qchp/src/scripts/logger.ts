/// <reference path="../index.d.ts" />

import * as ReadLine from "readline"
import { createWriteStream } from "graceful-fs"

const stdout = process.stdout
const logFile = createWriteStream(`${__dirname}/../../logs/${Date.now()}.txt`)
process.on("exit", () => logFile.end())

export const Logger = {

   TotalUrls: 0,
   DoneUrls: 0,
   
   WorkerErrors: 0,
   ErrorCount: 0,
   
   TotalWorkers: 0,
   DoneWorkers: 0,

   Clear(): void {
      ReadLine.moveCursor(stdout, 0, -7)
      ReadLine.cursorTo(stdout, 0)
      ReadLine.clearScreenDown(stdout)
   },

   LogProgress(): void {
      const speed = Math.round(Logger.DoneUrls / process.uptime() * 100)/100
      const stdMsg: Array<string> = [
         `Urls        : ${Logger.DoneUrls} / ${Logger.TotalUrls}`,
         `%Completion : ${parseInt((Logger.DoneUrls / Logger.TotalUrls * 100).toString())}%`,
         `Error Count : ${Logger.ErrorCount}`,
         `Workers     : ${Logger.DoneWorkers} / ${Logger.TotalWorkers}`,
         `Worker Errs : ${Logger.WorkerErrors}`,
         `Time Used   : ${Logger.GetTime(process.uptime())}`,
         `Speed       : ${speed} Urls/sec`,
         `Time left   : ${Logger.GetTime(Math.round((Logger.TotalUrls - Logger.DoneUrls) / speed))}`
      ]
      stdout.write(stdMsg.join("\n"))
   },

   Log(): void {
      Logger.Clear()
      Logger.LogProgress()
   },

   LogError(error: any): void {
      Logger.Clear()
      console.log(error)
      Logger.LogProgress()
      logFile.write(`${error}\n`)
   },

   GetTime(seconds: number): string {
      if (Number.isNaN(seconds) || !Number.isFinite(seconds)) return ""
      const date = new Date(null)
      date.setSeconds(seconds)
      return date.toUTCString().match(/[0-9]{2}\:[0-9]{2}\:[0-9]{2}/g)[0]
   }

}