/// <reference path="../index.d.ts" />

import { load } from "cheerio"
import { Promise } from "es6-promise" 
import * as request from "request"

String.prototype.strip = function (): string {
   return this.trim().replace(/\s{2,}/g, " ")
}

const Session = request.defaults(<request.CoreOptions>{
   jar: true,
   headers: {
      "User-agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.87 Safari/537.36"
   }
})

function Request(options: any): Promise<string> {
   return new Promise<string>((resolve, reject): void => {
      Session(options, (err: any, resp: request.RequestResponse, body: string): void => {
         if (err) {
            reject(err.code)
         } else if (resp.statusCode !== 200) {
            reject(resp.statusCode)
         } else {
            resolve(body)
         }
      })
   })
}

// define your scraping logic here
export function Scraper(code: string): Promise<Array<any>> {
   const url = `http://www.qchp.org.qa/en/_layouts/15/SCH_Website/PractitionerDetails.aspx?UserID=${code}&IsDlg=1&IsDlg=1`
   return Request(url)
      .then(Scrape.bind(null, code));
}

function Scrape(code: string, body: string): Array<any> {
   const $ = load(body)
   const rows: Array<any> = []
   
   const row: any = {
      code,
      issueDate: $("#PractitionerDetails1_lbl_issuedate").text().strip(),
      qualifications: $("#PractitionerDetails1_lbl_Qualification").text().strip()
   }

   rows.push(row)
   return rows
}