/// <reference path="../index.d.ts" />

import { Scraper } from "./scraper"
import { Promise } from "es6-promise"

const delayInMs = parseInt(process.argv[2]) * 1000

function SendNext(): void {
   process.send({action: "SendNext"})
}

function LogError(error: string, url: string): void {
   process.send({action: "LogError", error: error, url: url})
}

function SendData(rows: Array<any>): void {
   process.send({action: "Data", rows: rows})
}

function Wait(): Promise<void> {
   return new Promise<void>((resolve, reject) => setTimeout(resolve, delayInMs))
}

// Bring first url
SendNext()

process.on("message", (msg: msg) => {
   if (msg.action === "Scrape") {
      Scraper(msg.url)
         .then((rows: Array<any>) => SendData(rows))
         .catch((error) => LogError(error.toString(), msg.url))
         .then(Wait)
         .then(SendNext)
   } else if (msg.action === "KillWorker") {
      process.exit(0)
   }
})